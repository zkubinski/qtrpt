#include "mainwindow.h"
#include <QDebug>
#include <qtrpt.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    leText = new QLineEdit(this);
    leText->setText("Udało się");

    QtRPT *rap = new QtRPT(this);
    rap->loadReport(":/raport.xml");

    QObject::connect(rap, &QtRPT::setValue, [&](const int recNo, const QString paramName, QVariant &paramValue, const int reportPage)
    {
        (void) recNo;

        if(paramName == "wypelnic"){
            qDebug()<< leText->text();
            paramValue = leText->text();
        }
        else{
            qDebug()<< "coś poszło nie tak...";
        }

        (void) reportPage;
    });

    rap->printExec();
}

MainWindow::~MainWindow()
{
}

